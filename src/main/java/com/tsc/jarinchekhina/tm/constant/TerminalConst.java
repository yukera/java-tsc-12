package com.tsc.jarinchekhina.tm.constant;

public interface TerminalConst {

    String ABOUT = "about";

    String HELP = "help";

    String VERSION = "version";

    String INFO = "info";

    String COMMANDS = "commands";

    String ARGUMENTS = "arguments";

    String EXIT = "exit";

    String TASK_LIST = "task-list";

    String TASK_CREATE = "task-create";

    String TASK_CLEAR = "task-clear";

    String PROJECT_LIST = "project-list";

    String PROJECT_CREATE = "project-create";

    String PROJECT_CLEAR = "project-clear";

    String TASK_UPDATE_BY_INDEX = "task-update-by-index";

    String TASK_UPDATE_BY_ID = "task-update-by-id";

    String TASK_VIEW_BY_ID = "task-view-by-id";

    String TASK_VIEW_BY_INDEX = "task-view-by-index";

    String TASK_VIEW_BY_NAME = "task-view-by-name";

    String TASK_REMOVE_BY_ID = "task-remove-by-id";

    String TASK_REMOVE_BY_INDEX = "task-remove-by-index";

    String TASK_REMOVE_BY_NAME = "task-remove-by-name";

    String TASK_START_BY_ID = "task-start-by-id";

    String TASK_START_BY_INDEX = "task-start-by-index";

    String TASK_START_BY_NAME = "task-start-by-name";

    String TASK_FINISH_BY_ID = "task-finish-by-id";

    String TASK_FINISH_BY_INDEX = "task-finish-by-index";

    String TASK_FINISH_BY_NAME = "task-finish-by-name";

    String TASK_STATUS_CHANGE_BY_ID = "task-status-change-by-id";

    String TASK_STATUS_CHANGE_BY_INDEX = "task-status-change-by-index";

    String TASK_STATUS_CHANGE_BY_NAME = "task-status-change-by-name";

    String PROJECT_UPDATE_BY_INDEX = "project-update-by-index";

    String PROJECT_UPDATE_BY_ID = "project-update-by-id";

    String PROJECT_VIEW_BY_ID = "project-view-by-id";

    String PROJECT_VIEW_BY_INDEX = "project-view-by-index";

    String PROJECT_VIEW_BY_NAME = "project-view-by-name";

    String PROJECT_REMOVE_BY_ID = "project-remove-by-id";

    String PROJECT_REMOVE_BY_INDEX = "project-remove-by-index";

    String PROJECT_REMOVE_BY_NAME = "project-remove-by-name";

    String PROJECT_START_BY_ID = "project-start-by-id";

    String PROJECT_START_BY_INDEX = "project-start-by-index";

    String PROJECT_START_BY_NAME = "project-start-by-name";

    String PROJECT_FINISH_BY_ID = "project-finish-by-id";

    String PROJECT_FINISH_BY_INDEX = "project-finish-by-index";

    String PROJECT_FINISH_BY_NAME = "project-finish-by-name";

    String PROJECT_STATUS_CHANGE_BY_ID = "project-status-change-by-id";

    String PROJECT_STATUS_CHANGE_BY_INDEX = "project-status-change-by-index";

    String PROJECT_STATUS_CHANGE_BY_NAME = "project-status-change-by-name";

}
