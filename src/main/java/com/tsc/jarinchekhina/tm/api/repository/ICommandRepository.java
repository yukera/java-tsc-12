package com.tsc.jarinchekhina.tm.api.repository;

import com.tsc.jarinchekhina.tm.model.Command;

public interface ICommandRepository {

    Command[] getConstCommands();

}